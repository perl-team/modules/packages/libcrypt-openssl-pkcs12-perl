libcrypt-openssl-pkcs12-perl (1.94-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.94.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Oct 2024 19:14:53 +0200

libcrypt-openssl-pkcs12-perl (1.93-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.93.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Sep 2024 20:15:14 +0200

libcrypt-openssl-pkcs12-perl (1.92-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.92.
  * Update test dependencies.

 -- gregor herrmann <gregoa@debian.org>  Mon, 16 Sep 2024 21:21:26 +0200

libcrypt-openssl-pkcs12-perl (1.91-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.91.
  * Update years of upstream copyright.
  * debian/copyright: drop stanza about removed third-party files.
  * Update test and build dependencies.
  * Drop openssl3.patch (taken from upstream PR) and all the other parts
    of the framework to make it work.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Jul 2024 23:47:56 +0200

libcrypt-openssl-pkcs12-perl (1.9-3) unstable; urgency=medium

  * Team upload.
  * Update openssl3.patch to skip pkcs12 password test on all OpenSSL 3.x
    versions.
    Thanks to Sebastian Andrzej Siewior for the bug report.
    (Closes: #1052331)

 -- gregor herrmann <gregoa@debian.org>  Wed, 20 Sep 2023 19:43:10 +0200

libcrypt-openssl-pkcs12-perl (1.9-2) unstable; urgency=medium

  * Team upload.
  * Add patch from upstream pull request to fix tests with OpenSSL 3.
    (Closes: #1006386)
  * Handle the changed binary file for the tests from the pull request
    which can't be included in a quilt patch.
  * Add new test dependency libcrypt-openssl-guess-perl for the patched
    tests.
  * Use changed certs/test.p12 in autopkgtests as well.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Aug 2023 17:49:54 +0200

libcrypt-openssl-pkcs12-perl (1.9-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.9.

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Nov 2021 18:11:21 +0100

libcrypt-openssl-pkcs12-perl (1.8-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.8.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Tue, 16 Nov 2021 23:07:44 +0100

libcrypt-openssl-pkcs12-perl (1.7-1) unstable; urgency=medium

  * New upstream release (1.7)
  * Update copyright
  * Bump Standards-Version (4.5.0 to 4.5.1) - no change required

 -- Christopher Hoskin <mans0954@debian.org>  Sat, 10 Jul 2021 16:36:46 +0100

libcrypt-openssl-pkcs12-perl (1.3-1) unstable; urgency=medium

  * Team upload.

  [ Christopher Hoskin ]
  * Bump Standards-Version from 4.1.3 to 4.2.1 (no change required)
  * New upstream release (1.2)
  * Update copyright

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Replace '--with perl_openssl' in debian/rules with a build dependency
    on 'dh-sequence-perl-openssl'.

  * Import upstream version 1.3.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Tue, 09 Jun 2020 18:56:22 +0200

libcrypt-openssl-pkcs12-perl (1.0-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Christopher Hoskin ]
  * New upstream release (0.9)
  * Bump debhelper compat from 9 to 11

  [ gregor herrmann ]
  * Import upstream version 1.0.
  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 25 Mar 2018 19:27:38 +0200

libcrypt-openssl-pkcs12-perl (0.8-1) unstable; urgency=medium

  * New upstream release (0.8)
  * Use my Debian e-mail address in debian/control and debian/copyright
  * Remove openssl-1.1.patch (accepted upstream)
  * Bump Standards-Version from 3.9.8 to 4.1.1 (no change required)
  * Update debian/copyright

 -- Christopher Hoskin <mans0954@debian.org>  Sat, 25 Nov 2017 07:03:03 +0000

libcrypt-openssl-pkcs12-perl (0.7-3) unstable; urgency=medium

  * Team upload.
  * Dynamically generate a dependency on perl-openssl-abi-x.x
    to ensure compatibility with other OpenSSL-related Perl modules.
    (See #848113)

 -- Niko Tyni <ntyni@debian.org>  Tue, 03 Jan 2017 14:01:02 +0200

libcrypt-openssl-pkcs12-perl (0.7-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Christopher Hoskin ]
  * Fix "FTBFS with openssl 1.1.0" - patch for new API (Closes: #828386)
  * Enable hardening
  * Update Standards Version from 3.9.6 to 3.9.8 (no change required)
  * Use the simplified form of BTS URL in openssl-1.1.patch

 -- Christopher Hoskin <christopher.hoskin@gmail.com>  Sun, 06 Nov 2016 21:00:31 +0000

libcrypt-openssl-pkcs12-perl (0.7-1) unstable; urgency=low

  * New upstream release (0.7)

 -- Christopher Hoskin <christopher.hoskin@gmail.com>  Sat, 24 Oct 2015 11:18:24 +0100

libcrypt-openssl-pkcs12-perl (0.6-1) unstable; urgency=low

  * Initial Release. Closes: #801816

 -- Christopher Hoskin <christopher.hoskin@gmail.com>  Sat, 17 Oct 2015 13:59:01 +0100
